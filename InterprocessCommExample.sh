#!/bin/bash
# inspired by https://stackoverflow.com/questions/13207292/bash-background-process-modify-global-variable/13209088

function worker {
    initStateTransferReceive
    while true; do
        echo "working ($CHAR)"
        sleep 1
    done
}

##################
# HELPER METHODS #
##################

function initStateTransferSend {
    trap 'rm /dev/shm/$$' EXIT
    echo "" > /dev/shm/$$
}

function initStateTransferReceive {
    trap "receiveState" SIGUSR1
}

function sendState {
    echo "$1" > /dev/shm/$$
    kill -SIGUSR1 "$child"
}

function receiveState {
    CHAR="$(cat /dev/shm/$$)"
}

function initWorker {
    worker &
    child=$!
}

#########
# START #
#########

initStateTransferSend
initWorker
while true; do
    read -rsn1 char
    echo "Char $char pressed"
    sendState "$char"
done
